#!/usr/bin/env bash

cd "/tmp/nextcloud/apps" || exit 1

URL=$(jq -r '.[] | select(.id | match("^'"${APP}"'$")) | .releases | .[] | select(.version | match("^'"${APP_VERSION}"'$")) | .download' < /tmp/apps.json)
if [[ -n $URL ]]; then
    rm -rf "${APP:?}/"
    echo "Downloading ${APP} version ${APP_VERSION}"
    wget --quiet "$URL" -O "${APP}-${APP_VERSION}.tar.gz"
    tar xf "${APP}-${APP_VERSION}.tar.gz"
    rm "${APP}-${APP_VERSION}.tar.gz"
else
    cat <<EOF
=======================================================================
== Unable to find URL to download ${APP} version ${APP_VERSION}
=======================================================================
EOF
fi
