#!/usr/bin/env bash

RET=0

ASC_DIR=$1

apply_patch() {
    PATCH=$1
    ALREADY_PATCHED=$(patch --dry-run --forward --strip=1 --input="$PATCH" | grep -c -P 'Reversed \(or previously applied\) patch detected!  Skipping patch.|which already exists!  Skipping patch.')
    if [[ $ALREADY_PATCHED -eq 0 ]]
    then
        echo "Applying $PATCH"
        patch --backup --forward --strip=1 --input="$PATCH"
        PATCH_RET="$?"
        RET=$(( RET + PATCH_RET ))
        if [[ $? -ne 0 ]]; then
            cat <<EOF
====================================
Applying $PATCH returned $PATCH_RET
====================================
EOF
        fi
    else
        echo "Skipping $PATCH as it seems to be already applied"
    fi
}

# Nextcloud patches
cd /tmp/nextcloud || exit 1
for PATCH in "$ASC_DIR"/patches/nc/*; do
    apply_patch "$PATCH"
done

# App patches
for app in "$ASC_DIR"/patches/apps/*; do
    app=$(basename "$app")
    cd "/tmp/nextcloud/apps/$app" || exit 1
    if [[ -d "$ASC_DIR/patches/apps/$app/" ]]; then
        echo "===== Testing $app ====="
        for PATCH in "$ASC_DIR"/patches/apps/"$app"/*; do
            apply_patch "$PATCH"
        done
    fi
done

exit $RET
